package com.afpa.cda.exec;

import org.springframework.boot.SpringApplication;

import com.afpa.cda.ConfigurationApplication;

public class Program {
	public static void main(String[] args) {
		SpringApplication.run(ConfigurationApplication.class, args);
	}
}
